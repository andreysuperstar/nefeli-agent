/**
 * Agent that manages machine-level tasks for a UCPE or cluster node
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { SiteEntrySiteStatus } from './siteEntrySiteStatus';
import { SiteEntrySiteType } from './siteEntrySiteType';
import { SiteEntryWAN } from './siteEntryWAN';


export interface SiteEntry { 
    dataplaneVlanTag?: number;
    /**
     * `friendly_name` is a label that can have human-readable semantics. It can be set, and later edited, via the Dashboard or other REST client For example, 'Nefeli Sunnyvale Lanner'.
     */
    friendlyName?: string;
    hardwareProfileId?: string;
    identifier?: string;
    ip?: string;
    mac?: string;
    mgmtUrl?: string;
    mgmtVlanTag?: number;
    siteStatus?: SiteEntrySiteStatus;
    siteType?: SiteEntrySiteType;
    vniRangeEnd?: number;
    vniRangeStart?: number;
    wans?: Array<SiteEntryWAN>;
}

