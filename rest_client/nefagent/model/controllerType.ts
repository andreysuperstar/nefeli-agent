/**
 * Agent that manages machine-level tasks for a UCPE or cluster node
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export type ControllerType = 'NO_CONTROLLER' | 'NF_PROXY' | 'NF_CONTROLLER';

export const ControllerType = {
    NOCONTROLLER: 'NO_CONTROLLER' as ControllerType,
    NFPROXY: 'NF_PROXY' as ControllerType,
    NFCONTROLLER: 'NF_CONTROLLER' as ControllerType
};
