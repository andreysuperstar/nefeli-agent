/**
 * Agent that manages machine-level tasks for a UCPE or cluster node
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { Service } from './service';
import { TapestryExportNodeNFCConfig } from './tapestryExportNodeNFCConfig';


export interface TapestryExportServiceConfig { 
    configs?: Array<TapestryExportNodeNFCConfig>;
    id?: string;
    licensePoolId?: string;
    name?: string;
    policy?: Service;
    tenantId?: string;
}

