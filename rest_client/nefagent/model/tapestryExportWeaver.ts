/**
 * Agent that manages machine-level tasks for a UCPE or cluster node
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { AAAServerList } from './aAAServerList';
import { Attachment } from './attachment';
import { CustomizedUnit } from './customizedUnit';
import { HardwareProfile } from './hardwareProfile';
import { LicensePool } from './licensePool';
import { Logging } from './logging';
import { NFDescriptor } from './nFDescriptor';
import { ProvisioningURL } from './provisioningURL';
import { TapestryExportAlertsConfig } from './tapestryExportAlertsConfig';
import { TapestryExportCertKeyPair } from './tapestryExportCertKeyPair';
import { TapestryExportExpSite } from './tapestryExportExpSite';
import { TapestryExportLDAP } from './tapestryExportLDAP';
import { TapestryExportResource } from './tapestryExportResource';
import { TapestryExportServiceConfig } from './tapestryExportServiceConfig';
import { TapestryExportStoredFile } from './tapestryExportStoredFile';
import { TapestryExportTenant } from './tapestryExportTenant';
import { TapestryExportUser } from './tapestryExportUser';
import { TapestryExportWeaverMachine } from './tapestryExportWeaverMachine';
import { TunnelConfig } from './tunnelConfig';


export interface TapestryExportWeaver { 
    aaaServers?: AAAServerList;
    alertsConfig?: TapestryExportAlertsConfig;
    attachments?: Array<Attachment>;
    cephDiskSizeGb?: string;
    cephDiskThinProvisioned?: boolean;
    customizedUnits?: Array<CustomizedUnit>;
    disabledUnits?: Array<string>;
    externalCertKeyPair?: TapestryExportCertKeyPair;
    files?: Array<TapestryExportStoredFile>;
    floatingIpAddr?: string;
    fqdn?: string;
    hardwareProfiles?: Array<HardwareProfile>;
    ldapConfig?: TapestryExportLDAP;
    licensePools?: Array<LicensePool>;
    loggingConfig?: Logging;
    machines?: Array<TapestryExportWeaverMachine>;
    mgmtSans?: Array<string>;
    nfs?: Array<NFDescriptor>;
    provisioningUrls?: Array<ProvisioningURL>;
    services?: Array<TapestryExportServiceConfig>;
    sites?: Array<TapestryExportExpSite>;
    snapshot?: TapestryExportResource;
    sshUsername?: string;
    systemUsers?: Array<TapestryExportUser>;
    tacplusConfig?: string;
    tenants?: Array<TapestryExportTenant>;
    tunnels?: Array<TunnelConfig>;
    vaultData?: TapestryExportResource;
    ztpCertKeyPair?: TapestryExportCertKeyPair;
}

