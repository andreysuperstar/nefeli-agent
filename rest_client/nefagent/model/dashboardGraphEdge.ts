/**
 * Agent that manages machine-level tasks for a UCPE or cluster node
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { DashboardGraphEdgeDirection } from './dashboardGraphEdgeDirection';


export interface DashboardGraphEdge { 
    aGate?: string;
    aNode?: string;
    bGate?: string;
    bNode?: string;
    dirAb?: DashboardGraphEdgeDirection;
    dirBa?: DashboardGraphEdgeDirection;
    draining?: boolean;
}

