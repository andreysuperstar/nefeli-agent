/**
 * Agent that manages machine-level tasks for a UCPE or cluster node
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { InstanceStatus } from './instanceStatus';
import { ServiceNodeStatusState } from './serviceNodeStatusState';


export interface ServiceNodeStatus { 
    /**
     * If set, this NF has been frozen since this timestamp.
     */
    frozenSince?: Date;
    instances?: { [key: string]: InstanceStatus; };
    state?: ServiceNodeStatusState;
}

