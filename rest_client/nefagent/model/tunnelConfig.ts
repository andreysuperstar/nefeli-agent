/**
 * Agent that manages machine-level tasks for a UCPE or cluster node
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { LocalTunnel } from './localTunnel';
import { RemoteTunnel } from './remoteTunnel';


export interface TunnelConfig { 
    attachment?: string;
    id?: string;
    localTunnel?: LocalTunnel;
    name?: string;
    remoteTunnel?: RemoteTunnel;
    site?: string;
    tenant?: string;
}

