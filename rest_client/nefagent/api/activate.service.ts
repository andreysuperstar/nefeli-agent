/**
 * Agent that manages machine-level tasks for a UCPE or cluster node
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { ActivateError } from '../model/activateError';
import { AuthenticationError } from '../model/authenticationError';
import { DefaultResponse } from '../model/defaultResponse';
import { PackageError } from '../model/packageError';
import { SitePackage } from '../model/sitePackage';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable({
  providedIn: 'root'
})
export class ActivateService {

    protected basePath = 'http://http:/v1';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {

        if (configuration) {
            this.configuration = configuration;
            this.configuration.basePath = configuration.basePath || basePath || this.basePath;

        } else {
            this.configuration.basePath = basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * 
     * activates a UCPE using a site package, its checksum, and its passphrase
     * @param sitepackage 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postActivate(sitepackage: SitePackage, observe?: 'body', reportProgress?: boolean): Observable<DefaultResponse>;
    public postActivate(sitepackage: SitePackage, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<DefaultResponse>>;
    public postActivate(sitepackage: SitePackage, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<DefaultResponse>>;
    public postActivate(sitepackage: SitePackage, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (sitepackage === null || sitepackage === undefined) {
            throw new Error('Required parameter sitepackage was null or undefined when calling postActivate.');
        }

        let headers = this.defaultHeaders;

        // authentication (isAuthorized) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["Authorization"]) {
            headers = headers.set('Authorization', this.configuration.apiKeys["Authorization"]);
        }

        // to determine the Accept header
        const httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected !== undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected !== undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.post<DefaultResponse>(`${this.configuration.basePath}/activate`,
            sitepackage,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
