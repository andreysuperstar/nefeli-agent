export * from './activate.service';
import { ActivateService } from './activate.service';
export * from './health.service';
import { HealthService } from './health.service';
export * from './status.service';
import { StatusService } from './status.service';
export * from './upgradeAgent.service';
import { UpgradeAgentService } from './upgradeAgent.service';
export const APIS = [ActivateService, HealthService, StatusService, UpgradeAgentService];
