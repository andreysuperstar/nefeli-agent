import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ProvisioningStatus } from 'rest_client/nefagent/model/provisioningStatus';

type HttpServiceRequests = ProvisioningStatus | undefined;

@Injectable({
  providedIn: 'root'
})
export class RequestUrlInterceptor implements HttpInterceptor {

  constructor() { }

  public intercept(req: HttpRequest<HttpServiceRequests>, next: HttpHandler): Observable<HttpEvent<HttpServiceRequests>> {
    const api = environment.restServer;
    const reqUrl = req.url;
    let headers = req.headers;

    if (!req.headers.has('Content-Type')) {
      headers = req.headers.set('Content-Type', 'application/json');
    }

    req = req.clone({
      url: `${api}${reqUrl}`,
      headers
    });

    return next.handle(req);
  }
}
