import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ConfirmationDialogComponent, ConfirmationDialogData } from './confirmation-dialog.component';

describe('ConfirmationDialogComponent', () => {
  let component: ConfirmationDialogComponent;
  let fixture: ComponentFixture<ConfirmationDialogComponent>;
  // tslint:disable-next-line: no-any
  let el: any;
  const dialogData: ConfirmationDialogData = {
    title: 'dialog title',
    content: 'dialog contents',
    buttons: [{
      label: 'button 1',
      callback: () => {
        console.log('button 1 clicked');
      }
    }, {
      label: 'button 2',
      callback: () => {
        console.log('button 2 clicked');
      }
    }]
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatDialogModule],
      declarations: [ConfirmationDialogComponent],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: dialogData
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display title and content', () => {
    expect(el.querySelector('h1[mat-dialog-title]').textContent).toBe('dialog title');
    expect(el.querySelector('mat-dialog-content').textContent).toBe('dialog contents');
  });

  it('should display buttons and assign action', () => {
    spyOn(console, 'log');
    expect(el.querySelectorAll('mat-dialog-actions > button').length).toBe(3);

    const button1: HTMLButtonElement = el.querySelectorAll('mat-dialog-actions > button')[0];
    const button2: HTMLButtonElement = el.querySelectorAll('mat-dialog-actions > button')[1];
    const closeButton: HTMLButtonElement = el.querySelector('mat-dialog-actions > button[mat-dialog-close]');

    expect(button1.textContent).toBe('button 1');
    expect(button2.textContent).toBe('button 2');
    expect(closeButton.textContent).toBe('Close');

    button1.click();
    expect(console.log).toHaveBeenCalledWith('button 1 clicked');
    button2.click();
    expect(console.log).toHaveBeenCalledWith('button 2 clicked');
  });
});
