import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface ConfirmationDialogButton {
  label: string;
  callback: () => void;
}

export interface ConfirmationDialogData {
  title: string;
  content: string;
  buttons: ConfirmationDialogButton[];
}

@Component({
  selector: 'nefagent-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.less']
})
export class ConfirmationDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) private _data: ConfirmationDialogData) { }

  public get title(): string {
    return this._data?.title;
  }

  public get content(): string {
    return this._data?.content;
  }

  public get buttons(): ConfirmationDialogButton[] {
    return this._data?.buttons;
  }
}
