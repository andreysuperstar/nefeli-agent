export enum HttpStatus {
  BadRequest = 400,
  Unauthorized = 401,
  UnprocessableEntity = 422,
  InternalServerError = 500
}
