import { environment } from '../../environments/environment';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { ProvisionService } from './provision.service';
import { BASE_PATH } from 'rest_client/nefagent/variables';
import { ProvisioningStatus } from 'rest_client/nefagent/model/provisioningStatus';
import { ProvisioningStatusState } from '../../../rest_client/nefagent/model/provisioningStatusState';

const mockStatusResponse: ProvisioningStatus = {
  state: ProvisioningStatusState.BOOTSTRAPINSTALLED,
  version: '0.1'
};

describe('ProvisionService', () => {
  let service: ProvisionService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatToolbarModule
      ],
      providers: [
        ProvisionService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        }
      ]
    });
    service = TestBed.inject(ProvisionService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should poll status', fakeAsync (() => {
    const subscription = service.streamStatus().subscribe((status: ProvisioningStatus | undefined) => {
      expect(status).toBeDefined();
      expect(status?.state).toBe(ProvisioningStatusState.BOOTSTRAPINSTALLED);
    });

    const req0 = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    req0.flush(mockStatusResponse);

    tick(3000);

    const req1 = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    req1.flush(mockStatusResponse);

    subscription.unsubscribe();
    flush();
  }));
});
