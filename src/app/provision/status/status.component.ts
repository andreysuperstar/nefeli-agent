import { EMPTY, merge, Observable, of, Subscription, timer } from 'rxjs';
import { catchError, delay, find, mergeMapTo, retryWhen, switchMap, tap } from 'rxjs/operators';
import { Component, OnInit, ViewChild, EventEmitter, Output, OnDestroy, Input } from '@angular/core';
import { MatVerticalStepper } from '@angular/material/stepper';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { ProvisionService } from '../provision.service';
import { ProvisioningStatus } from 'rest_client/nefagent/model/provisioningStatus';
import { ProvisioningStatusState } from 'rest_client/nefagent/model/provisioningStatusState';
import { MODE } from '../completion/completion.component';
import { UpgradeAgentResponse } from 'rest_client/nefagent/model/upgradeAgentResponse';
import { ActivateService } from 'rest_client/nefagent/api/activate.service';
import { HealthService } from 'rest_client/nefagent/api/health.service';
import { AgentHealth } from 'rest_client/nefagent/model/agentHealth';
import { PackageInfo } from '../package-upload/package-upload.component';
import { MatDialog, MatDialogRef, MatDialogState } from '@angular/material/dialog';
import { ConfirmationDialogComponent, ConfirmationDialogData } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { MatSnackBar, MatSnackBarRef, TextOnlySnackBar } from '@angular/material/snack-bar';
import { TabIndex } from '../provision.component';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpStatus } from 'src/app/http-status';
import { ActivateError } from 'rest_client/nefagent/model/activateError';

export interface StatusCompleteEvent {
  mode: MODE;
}

const Config = {
  POLL_INTERVAL: 3000,
  RESTART_TIME_ON_ERROR: 60 * 1000,
  ZTP_CONNECTING_ATTEMPTS: 3,
  MAX_HEALTH_FAILURES: 100
};

enum StatusStep {
  INSTALLING_NEFAGENT = 0,
  INSTALLING_BOOTSTRAP = 1,
  CONTACTING_PROVISIONING_SERVER = 2,
  INSTALLING_SERVICES = 3,
  CONFIGURING_DEVICE = 4
}

interface ConnectingState {
  provisionServerUrls: Set<string>;
  prevFailTime?: Date | undefined;
  errCount: number;
}

export const POLL_INTERVAL = 3000;

@Component({
  selector: 'nefagent-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.less'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { showError: true }
    }
  ]
})
export class StatusComponent implements OnInit, OnDestroy {
  @ViewChild('stepper', { static: true }) private _stepper?: MatVerticalStepper;
  private _subscription = new Subscription();
  private _statusComplete = new EventEmitter<StatusCompleteEvent>();
  private _hasRollbackError = false;
  private _pkgInfo: PackageInfo | undefined;
  private _connectWarnDialog: MatDialogRef<ConfirmationDialogData> | undefined;
  private _activationErrDialog: MatDialogRef<ConfirmationDialogData> | undefined;
  private _healthErrDialog: MatDialogRef<ConfirmationDialogData> | undefined;
  private _goToTab = new EventEmitter<TabIndex>();
  private _connectingState: ConnectingState = {
    provisionServerUrls: new Set<string>(),
    errCount: 0
  };
  private _consecutiveHealthErrors = 0;
  private _provisioningAlreadyInProgress = false;
  private _matSnackbarRef?: MatSnackBarRef<TextOnlySnackBar>;
  private _showRestartSnackbar = true;

  @Output() public get statusComplete(): EventEmitter<StatusCompleteEvent> {
    return this._statusComplete;
  }

  @Output() public get goToTab(): EventEmitter<TabIndex> {
    return this._goToTab;
  }

  @Input() public set pkgInfo(val: PackageInfo | undefined) {
    this._pkgInfo = val;
  }

  @Input() public set provisioningAlreadyInProgress(val: boolean) {
    this._provisioningAlreadyInProgress = val;
  }

  @Input() public set showRestartSnackbar(val: boolean) {
    this._showRestartSnackbar = val;
  }

  constructor(
    private _provisionService: ProvisionService,
    private _activateService: ActivateService,
    private _healthService: HealthService,
    private _snackBar: MatSnackBar,
    public _dialog: MatDialog
  ) { }

  public ngOnInit(): void {
    if (this._pkgInfo) {
      this._subscription.add(this.activateDevice(this._pkgInfo).subscribe());
    } else if (this._provisioningAlreadyInProgress) {
      if (this._showRestartSnackbar) {
        this.openRestartSnackBar();
      }
      this.startStatusStream();
    }
  }

  public ngOnDestroy(): void {
    this._subscription.unsubscribe();
    this._activationErrDialog?.close();
    this._healthErrDialog?.close();
  }

  private handleActivateError(error: HttpErrorResponse): void {
    const response = error.error;
    let content: string;
    let buttonLabel: string;

    switch (error.status) {
      case HttpStatus.Unauthorized:
        content = 'Passphrase is incorrect, please try again.';
        buttonLabel = 'Try again';
        break;
      case HttpStatus.UnprocessableEntity:
        if (response.errorCode === ActivateError.ErrorCodeEnum.NUMBER_114) {
          content = 'Device is already activated. If you find this to be incorrect, try regenerating the package file.';
          buttonLabel = 'Upload new package';
        } else {
          content = 'Invalid package. Regenerate the package file and try again.';
          buttonLabel = 'Upload new package';
        }
        break;
      case HttpStatus.InternalServerError:
      case HttpStatus.BadRequest:
      default:
        content = 'Could not activate device, please try again. If you continue to have problems, try regenerating the package file.';
        buttonLabel = 'Re-upload package';
    }

    this._activationErrDialog = this._dialog.open<ConfirmationDialogComponent, ConfirmationDialogData>(ConfirmationDialogComponent, {
      data: {
        title: 'Activation Error',
        content,
        buttons: [{
          label: buttonLabel,
          callback: () => this._goToTab.emit(TabIndex.UPLOAD)
        }]
      }
    });
  }

  private handleHealthError(error: HttpErrorResponse): void {
    if (this._healthErrDialog?.getState() !== MatDialogState.OPEN) {
      this._healthErrDialog = this._dialog.open<ConfirmationDialogComponent, ConfirmationDialogData>(ConfirmationDialogComponent, {
        data: {
          title: 'Activation Error',
          content: 'Unexpected failure, please try again. If you continue to have problems, try regenerating the package file.',
          buttons: [{
            label: 'Try again',
            callback: () => this._goToTab.emit(TabIndex.UPLOAD)
          }]
        }
      });
    }
  }

  private activateDevice(pkgInfo: PackageInfo): Observable<AgentHealth | never | void> {
    const activate$ = merge(
      this._activateService.postActivate(pkgInfo.sitePackage).pipe(
        catchError((error: HttpErrorResponse) => {
          this.handleActivateError(error);
          return EMPTY;
        })
      ),
      of(this.startStatusStream()));

    switch (pkgInfo.disposition) {
      case UpgradeAgentResponse.DispositionEnum.NUMBER_1:
        return activate$;
      case UpgradeAgentResponse.DispositionEnum.NUMBER_2:
        const health$ = timer(0, POLL_INTERVAL)
          .pipe(
            switchMap(() => this._healthService.getHealth()),
            retryWhen(errors => errors.pipe(
              tap((error: HttpErrorResponse) => {
                if (++this._consecutiveHealthErrors > Config.MAX_HEALTH_FAILURES) {
                  this.handleHealthError(error);
                }
              }),
              delay(POLL_INTERVAL)
            ))
          );

        const upgraded$ = health$.pipe(
          tap(() => {
            this._consecutiveHealthErrors = 0;
            this._healthErrDialog?.close();
          }),
          find(({ version }: AgentHealth) => version === pkgInfo.wantVersion)
        );

        return upgraded$.pipe(
          mergeMapTo(activate$)
        );
      default:
        return EMPTY;
    }
  }

  private openConnectionWarningDialog(servers: Set<string>): void {
    if (this._connectWarnDialog?.getState() !== MatDialogState.OPEN) {
      this._connectWarnDialog = this._dialog.open<ConfirmationDialogComponent, ConfirmationDialogData>(ConfirmationDialogComponent, {
        data: {
          title: 'Connection Warning',
          content: `Encountering problems connecting to the following provisioning server${servers.size > 1 ? 's' : ''}: ${Array.from(servers)?.map((server: string) => `<ul>${server}</ul>`)?.join('')}`,
          buttons: [{
            label: 'Upload new package',
            callback: () => {
              this._goToTab.emit(TabIndex.UPLOAD);
              this._connectWarnDialog?.close();
            }
          }]
        }
      });
    }
  }

  private checkConnectionStatus(status: ProvisioningStatus): void {
    if (status.additionalInfo === 'ztp onboard unsuccessful, will retry') {
      if (status.eventTime &&
        (!this._connectingState.prevFailTime ||
          this._connectingState.prevFailTime !== status.eventTime)) {
        if (++this._connectingState.errCount >= Config.ZTP_CONNECTING_ATTEMPTS) {
          this.openConnectionWarningDialog(this._connectingState.provisionServerUrls);
        }
      }

      this._connectingState.prevFailTime = status.eventTime;

    } else if (status.additionalInfo?.startsWith('http')) {
      this._connectingState.provisionServerUrls.add(status.additionalInfo);
    }
  }

  private startStatusStream(): void {
    const subscription = this._provisionService
      .streamStatus()
      .pipe(
        retryWhen(errors => errors.pipe(
          delay(Config.POLL_INTERVAL)
        ))
      )
      .subscribe((status: ProvisioningStatus) => {
        if (status.state !== ProvisioningStatusState.ZTPROLLINGBACK) {
          this._hasRollbackError = false;
        }

        const state: ProvisioningStatusState = status.state as ProvisioningStatusState;
        if (this._stepper) {
          switch (state) {
            case ProvisioningStatusState.NULLPROVISIONINGSTATE: {
              this._stepper.selectedIndex = StatusStep.INSTALLING_NEFAGENT;
              break;
            }
            case ProvisioningStatusState.ACTIVATIONINITIATED: {
              this._stepper.selectedIndex = StatusStep.INSTALLING_BOOTSTRAP;
              break;
            }
            case ProvisioningStatusState.BOOTSTRAPINSTALLED: {
              this._stepper.selectedIndex = StatusStep.CONTACTING_PROVISIONING_SERVER;
              break;
            }
            case ProvisioningStatusState.ZTPCONNECTING: {
              this._stepper.selectedIndex = StatusStep.CONTACTING_PROVISIONING_SERVER;
              this.checkConnectionStatus(status);
              break;
            }
            case ProvisioningStatusState.ZTPINSTALLING: {
              /* connecting to provisioning server has succeeded, so close connecting warning dialog if exists */
              this._connectWarnDialog?.close();

              this._stepper.selectedIndex = StatusStep.INSTALLING_SERVICES;
              break;
            }
            case ProvisioningStatusState.ZTPCONFIGURING: {
              this._stepper.selectedIndex = StatusStep.CONFIGURING_DEVICE;
              break;
            }
            case ProvisioningStatusState.ZTPSUCCESS: {
              // redirect to success page
              this._statusComplete.emit({ mode: MODE.SUCCESS });
              break;
            }
            case ProvisioningStatusState.ZTPROLLINGBACK: {
              this._hasRollbackError = true;
              break;
            }
            case ProvisioningStatusState.ZTPFATAL: {

              // redirect to error page
              this._statusComplete.emit({ mode: MODE.ERROR });
              break;
            }
          }
        }

        if (this._provisionService.isPassedConnectingState(state)) {
          this.dismissSnackbar();
        }
      });
    this._subscription.add(subscription);
  }

  private openRestartSnackBar(): void {
    this._matSnackbarRef = this._snackBar.open('Upload new package file and restart provisioning', 'Return');

    this._matSnackbarRef.onAction().subscribe(() => {
      this.dismissSnackbar();
      this._goToTab.emit(TabIndex.UPLOAD);
    });
  }

  public dismissSnackbar(): void {
    this._matSnackbarRef?.dismiss();
  }

  public get currentStepIndex(): number {
    return this._stepper?.selectedIndex ?? 0;
  }

  public get hasRollbackError(): boolean {
    return this._hasRollbackError;
  }

}
