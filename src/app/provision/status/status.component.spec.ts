import { cloneDeep } from 'lodash';
import { ComponentFixture, TestBed, fakeAsync, tick, flush } from '@angular/core/testing';
import { EventEmitter } from '@angular/core';
import { StatusComponent, StatusCompleteEvent, POLL_INTERVAL } from './status.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatStepperModule } from '@angular/material/stepper';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule, MatSnackBarRef, TextOnlySnackBar } from '@angular/material/snack-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ToolbarComponent } from '../../toolbar/toolbar.component';
import { CompletionComponent } from '../completion/completion.component';
import { HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';
import { ProvisionService } from '../provision.service';
import { environment } from '../../../environments/environment';
import { BASE_PATH } from 'rest_client/nefagent/variables';
import { ProvisioningStatus } from 'rest_client/nefagent/model/provisioningStatus';
import { ProvisioningStatusState } from 'rest_client/nefagent/model/provisioningStatusState';
import { MODE } from '../completion/completion.component';
import { UpgradeAgentResponse } from 'rest_client/nefagent/model/upgradeAgentResponse';
import { DefaultResponse } from 'rest_client/nefagent/model/defaultResponse';
import { AgentHealth } from 'rest_client/nefagent/model/agentHealth';
import { MatDialogModule, MatDialogRef, MatDialogState } from '@angular/material/dialog';
import { ConfirmationDialogButton, ConfirmationDialogData, ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { TabIndex } from '../provision.component';
import { PackageError } from 'rest_client/nefagent/model/packageError';
import { HtmlParser } from '@angular/compiler';

const mockStatusResponse: ProvisioningStatus = {
  state: ProvisioningStatusState.NULLPROVISIONINGSTATE,
  version: '0.1'
};

const snackbarRefSpy: MatSnackBarRef<TextOnlySnackBar> = jasmine.createSpyObj('ref',
  {
    onAction: {
      subscribe: () => { }
    },
    dismiss: () => { }
  });

describe('StatusComponent', () => {
  let component: StatusComponent;
  let fixture: ComponentFixture<StatusComponent>;
  let httpTestingController: HttpTestingController;
  let provisionService: ProvisionService;
  let el: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatToolbarModule,
        MatStepperModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatSnackBarModule
      ],
      declarations: [
        StatusComponent,
        ToolbarComponent,
        CompletionComponent,
        ConfirmationDialogComponent
      ],
      providers: [
        ProvisionService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        }
      ]
    })
      .compileComponents();

    provisionService = TestBed.inject(ProvisionService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement.nativeElement;

    const pkgData = 'package-data';
    const pkgPassphrase = 'passphrase-data';

    component.pkgInfo = {
      disposition: UpgradeAgentResponse.DispositionEnum.NUMBER_1,
      wantVersion: '0.0.1',
      sitePackage: {
        encryptedPackage: pkgData,
        passphrase: pkgPassphrase
      }
    };
    component.ngOnInit();

    const activateRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/activate`
    });
    expect(activateRequest.request.body.encryptedPackage).toBe(pkgData);
    expect(activateRequest.request.body.passphrase).toBe(pkgPassphrase);

    activateRequest.flush({
      status: DefaultResponse.StatusEnum.Success
    });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have installation steps', () => {
    const steps = el.querySelectorAll('mat-vertical-stepper .mat-step');
    expect(steps.length).toBe(5);
    expect(steps[0]?.querySelector('.mat-step-label')?.textContent?.trim()).toBe('Upgrading Nefeli agent software');
    expect(steps[1]?.querySelector('.mat-step-label')?.textContent?.trim()).toBe('Installing bootstrap package');
    expect(steps[2]?.querySelector('.mat-step-label')?.textContent?.trim()).toBe('Contacting provisioning server');
    expect(steps[3]?.querySelector('.mat-step-label')?.textContent?.trim()).toBe('Installing services');
    expect(steps[4]?.querySelector('.mat-step-label')?.textContent?.trim()).toBe('Configuring device');
  });

  it('should set installing nefagent step', () => {
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    expect(request.request.method).toBe('GET');
    const mockStatus = cloneDeep(mockStatusResponse);
    mockStatus.state = ProvisioningStatusState.NULLPROVISIONINGSTATE;
    request.flush(mockStatus);
    fixture.detectChanges();

    const steps = el.querySelectorAll('mat-vertical-stepper .mat-step');
    const stepIconEl = steps[0]?.querySelector('.mat-step-icon');
    expect(stepIconEl?.classList).toContain('mat-step-icon-selected');

    const doneSteps = el.querySelectorAll('mat-vertical-stepper .mat-step-icon-state-done');
    expect(doneSteps.length).toBe(0);
  });

  it('should set installing bootstrap package step', () => {
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    const mockStatus = cloneDeep(mockStatusResponse);
    mockStatus.state = ProvisioningStatusState.ACTIVATIONINITIATED;
    request.flush(mockStatus);
    fixture.detectChanges();

    const steps = el.querySelectorAll('mat-vertical-stepper .mat-step');
    const stepIconEl = steps[1]?.querySelector('.mat-step-icon');
    expect(stepIconEl?.classList).toContain('mat-step-icon-selected');

    const doneSteps = el.querySelectorAll('mat-vertical-stepper .mat-step-icon-state-done');
    expect(doneSteps.length).toBe(1);
  });

  it('should set contacting provisioning server step', () => {
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    const mockStatus = cloneDeep(mockStatusResponse);
    mockStatus.state = ProvisioningStatusState.BOOTSTRAPINSTALLED;
    request.flush(mockStatus);
    fixture.detectChanges();

    const steps = el.querySelectorAll('mat-vertical-stepper .mat-step');
    const stepIconEl = steps[2]?.querySelector('.mat-step-icon');
    expect(stepIconEl?.classList).toContain('mat-step-icon-selected');

    const doneSteps = el.querySelectorAll('mat-vertical-stepper .mat-step-icon-state-done');
    expect(doneSteps.length).toBe(2);
  });

  it('should set contacting provisioning server step on ztp connecting status', () => {
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    const mockStatus = cloneDeep(mockStatusResponse);
    mockStatus.state = ProvisioningStatusState.ZTPCONNECTING;
    request.flush(mockStatus);
    fixture.detectChanges();

    const steps = el.querySelectorAll('mat-vertical-stepper .mat-step');
    const stepIconEl = steps[2]?.querySelector('.mat-step-icon');
    expect(stepIconEl?.classList).toContain('mat-step-icon-selected');

    const doneSteps = el.querySelectorAll('mat-vertical-stepper .mat-step-icon-state-done');
    expect(doneSteps.length).toBe(2);
  });

  it('should set installing device step', () => {
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    const mockStatus = cloneDeep(mockStatusResponse);
    mockStatus.state = ProvisioningStatusState.ZTPINSTALLING;
    request.flush(mockStatus);
    fixture.detectChanges();

    const steps = el.querySelectorAll('mat-vertical-stepper .mat-step');
    const stepIconEl = steps[3]?.querySelector('.mat-step-icon');
    expect(stepIconEl?.classList).toContain('mat-step-icon-selected');

    const doneSteps = el.querySelectorAll('mat-vertical-stepper .mat-step-icon-state-done');
    expect(doneSteps.length).toBe(3);
  });

  it('should set configuring device step', () => {
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    const mockStatus = cloneDeep(mockStatusResponse);
    mockStatus.state = ProvisioningStatusState.ZTPCONFIGURING;
    request.flush(mockStatus);
    fixture.detectChanges();

    const steps = el.querySelectorAll('mat-vertical-stepper .mat-step');
    const stepIconEl = steps[4]?.querySelector('.mat-step-icon');
    expect(stepIconEl?.classList).toContain('mat-step-icon-selected');

    const doneSteps = el.querySelectorAll('mat-vertical-stepper .mat-step-icon-state-done');
    expect(doneSteps.length).toBe(4);
  });

  it('should display rollback error', fakeAsync(() => {
    const req2 = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    const mockStatus = cloneDeep(mockStatusResponse);
    mockStatus.state = ProvisioningStatusState.ZTPINSTALLING;
    req2.flush(mockStatus);
    fixture.detectChanges();

    const steps = el.querySelectorAll('mat-vertical-stepper .mat-step');
    let stepError = steps[3].querySelector('.step-error');
    expect(stepError).toBeNull();
    fixture.detectChanges();

    tick(3000);
    const req1 = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    mockStatus.state = ProvisioningStatusState.ZTPROLLINGBACK;
    req1.flush(mockStatus);
    fixture.detectChanges();

    stepError = steps[3].querySelector('.step-error');
    expect(stepError?.textContent).toBe('Could not provision device, retrying…');
    flush();
    component.ngOnDestroy();
  }));

  it('should redirect to error page on fatal response', () => {
    spyOn<EventEmitter<StatusCompleteEvent>>(component.statusComplete, 'emit').and.callThrough();
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    const mockStatus = cloneDeep(mockStatusResponse);
    mockStatus.state = ProvisioningStatusState.ZTPFATAL;
    request.flush(mockStatus);
    fixture.detectChanges();
    expect(component.statusComplete.emit).toHaveBeenCalledWith({ mode: MODE.ERROR });
  });

  it('should redirect to success page', () => {
    spyOn<EventEmitter<StatusCompleteEvent>>(component.statusComplete, 'emit').and.callThrough();
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    const mockStatus = cloneDeep(mockStatusResponse);
    mockStatus.state = ProvisioningStatusState.ZTPSUCCESS;
    request.flush(mockStatus);
    fixture.detectChanges();
    expect(component.statusComplete.emit).toHaveBeenCalledWith({ mode: MODE.SUCCESS });
  });

  it('should resume poll after server error response', fakeAsync(() => {
    spyOn<EventEmitter<StatusCompleteEvent>>(component.statusComplete, 'emit').and.callThrough();
    const failedReq = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    failedReq.flush({}, {
      status: 500,
      statusText: 'Server error',
    });

    tick(3000);

    // expected to resume polling after polling interval
    const resumedReq = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    const mockStatus = cloneDeep(mockStatusResponse);
    mockStatus.state = ProvisioningStatusState.ZTPSUCCESS;
    resumedReq.flush(mockStatus);
    fixture.detectChanges();
    expect(component.statusComplete.emit).toHaveBeenCalledWith({ mode: MODE.SUCCESS });
    flush();
    component.ngOnDestroy();
  }));

  it('should poll health until desired version', fakeAsync(() => {
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush({});
    component.ngOnDestroy();

    fixture = TestBed.createComponent(StatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const pkgData = 'package-data';
    const pkgPassphrase = 'passphrase-data';

    component.pkgInfo = {
      disposition: UpgradeAgentResponse.DispositionEnum.NUMBER_2,
      wantVersion: '0.0.2',
      sitePackage: {
        encryptedPackage: pkgData,
        passphrase: pkgPassphrase
      }
    };
    component.ngOnInit();

    tick();

    // respond with out of date package version
    let healthRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/health`
    });

    let health: AgentHealth = {
      status: 'success',
      version: '0.0.1'
    };
    healthRequest.flush(health);

    tick(POLL_INTERVAL);

    // respond with an offline network error
    healthRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/health`
    });

    const errorResponse: Partial<HttpErrorResponse> = {
      status: 0,
      statusText: 'Cancelled'
    };

    healthRequest.flush({}, errorResponse);

    tick(POLL_INTERVAL);

    // respond with an upgraded package version
    healthRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/health`
    });

    health = {
      status: 'success',
      version: '0.0.2'
    };
    healthRequest.flush(health);

    // activate ucpe
    const activateRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/activate`
    });

    activateRequest.flush({
      status: DefaultResponse.StatusEnum.Success
    });

    // should now be polling for status
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush({});
    component.ngOnDestroy();

    flush();
  }));

  it('should display connection warning dialog', fakeAsync(() => {
    const connectFailure = 'ztp onboard unsuccessful, will retry';
    const provisioningServers = ['https://provision1.server.com', 'https://provision2.server.com', 'https://provision3.server.com'];
    const mockStatus = cloneDeep(mockStatusResponse);
    mockStatus.state = ProvisioningStatusState.ZTPCONNECTING;

    mockStatus.additionalInfo = provisioningServers[0];
    mockStatus.eventTime = new Date();
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush(mockStatus);
    tick(POLL_INTERVAL);

    mockStatus.additionalInfo = connectFailure;
    mockStatus.eventTime = new Date();
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush(mockStatus);
    tick(POLL_INTERVAL);

    mockStatus.additionalInfo = provisioningServers[1];
    mockStatus.eventTime = new Date();
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush(mockStatus);
    tick(POLL_INTERVAL);

    mockStatus.additionalInfo = connectFailure;
    mockStatus.eventTime = new Date();
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush(mockStatus);
    tick(POLL_INTERVAL);

    mockStatus.additionalInfo = provisioningServers[2];
    mockStatus.eventTime = new Date();
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush(mockStatus);
    tick(POLL_INTERVAL);

    mockStatus.additionalInfo = connectFailure;
    mockStatus.eventTime = new Date();
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush(mockStatus);
    tick(POLL_INTERVAL);
    fixture.detectChanges();

    spyOn(component.goToTab, 'emit');

    expect(component['_connectWarnDialog']).toBeDefined();
    if (component['_connectWarnDialog']) {
      expect(component['_connectWarnDialog'].getState()).toBe(MatDialogState.OPEN);

      const dialogComponent = component['_connectWarnDialog'].componentInstance;
      expect(dialogComponent.title).toBe('Connection Warning');
      expect(dialogComponent.content).toBe('Encountering problems connecting to the following provisioning servers: <ul>https://provision1.server.com</ul><ul>https://provision2.server.com</ul><ul>https://provision3.server.com</ul>');

      const dialogButton: ConfirmationDialogButton = dialogComponent.buttons[0];
      expect(dialogButton.label).toBe('Upload new package');
      dialogButton.callback();
      expect(component.goToTab.emit).toHaveBeenCalledWith(TabIndex.UPLOAD);
    }
  }));

  it('should properly handle /activate errors', () => {
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush({});
    component.ngOnDestroy();

    fixture = TestBed.createComponent(StatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.pkgInfo = {
      disposition: UpgradeAgentResponse.DispositionEnum.NUMBER_1,
      wantVersion: '0.0.2',
      sitePackage: {
        encryptedPackage: 'package-data',
        passphrase: 'passphrase-data'
      }
    };
    component.ngOnInit();

    // activate ucpe
    const activateRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/activate`
    });

    // test 400 error
    const packageErr: PackageError = {
      httpStatus: 400,
      message: 'Generic unspecified error.',
      errorCode: PackageError.ErrorCodeEnum.NUMBER_400
    };
    activateRequest.flush(packageErr, { status: 400, statusText: '' });

    spyOn(component.goToTab, 'emit');
    let dialog = component['_activationErrDialog'] as MatDialogRef<ConfirmationDialogData>;
    expect(dialog.getState()).toBe(MatDialogState.OPEN);
    expect(dialog.componentInstance.content).toBe(
      'Could not activate device, please try again. If you continue to have problems, try regenerating the package file.');
    expect(dialog.componentInstance.title).toBe('Activation Error');
    expect(dialog.componentInstance.buttons[0].label).toBe('Re-upload package');
    dialog.componentInstance.buttons[0].callback();
    expect(component.goToTab.emit).toHaveBeenCalledWith(TabIndex.UPLOAD);
    dialog.close();

    // 401 error
    component['handleActivateError']({
      status: 401,
    } as HttpErrorResponse);
    fixture.detectChanges();
    dialog = component['_activationErrDialog'] as MatDialogRef<ConfirmationDialogData>;
    expect(dialog.getState()).toBe(MatDialogState.OPEN);
    expect(dialog.componentInstance.content).toBe(
      'Passphrase is incorrect, please try again.');
    expect(dialog.componentInstance.title).toBe('Activation Error');
    expect(dialog.componentInstance.buttons[0].label).toBe('Try again');
    dialog.componentInstance.buttons[0].callback();
    expect(component.goToTab.emit).toHaveBeenCalledWith(TabIndex.UPLOAD);
    dialog.close();

    // 422 error (already activated)
    component['handleActivateError']({
      status: 422,
      error: {
        errorCode: 114
      }
    } as HttpErrorResponse);
    fixture.detectChanges();
    dialog = component['_activationErrDialog'] as MatDialogRef<ConfirmationDialogData>;
    expect(dialog.getState()).toBe(MatDialogState.OPEN);
    expect(dialog.componentInstance.content).toBe(
      'Device is already activated. If you find this to be incorrect, try regenerating the package file.');
    expect(dialog.componentInstance.title).toBe('Activation Error');
    expect(dialog.componentInstance.buttons[0].label).toBe('Upload new package');
    dialog.componentInstance.buttons[0].callback();
    expect(component.goToTab.emit).toHaveBeenCalledWith(TabIndex.UPLOAD);
    dialog.close();

    // 422 error
    component['handleActivateError']({
      status: 422,
      error: {
        errorCode: 100
      }
    } as HttpErrorResponse);
    fixture.detectChanges();
    dialog = component['_activationErrDialog'] as MatDialogRef<ConfirmationDialogData>;
    expect(dialog.getState()).toBe(MatDialogState.OPEN);
    expect(dialog.componentInstance.content).toBe(
      'Invalid package. Regenerate the package file and try again.');
    expect(dialog.componentInstance.title).toBe('Activation Error');
    expect(dialog.componentInstance.buttons[0].label).toBe('Upload new package');
    dialog.componentInstance.buttons[0].callback();
    expect(component.goToTab.emit).toHaveBeenCalledWith(TabIndex.UPLOAD);
    dialog.close();

    component.ngOnDestroy();
  });

  it('should properly handle /health errors', fakeAsync(() => {
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush({});
    component.ngOnDestroy();

    fixture = TestBed.createComponent(StatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.pkgInfo = {
      disposition: UpgradeAgentResponse.DispositionEnum.NUMBER_2,
      wantVersion: '0.0.2',
      sitePackage: {
        encryptedPackage: 'package-data',
        passphrase: 'passphrase-data'
      }
    };
    component.ngOnInit();

    tick();

    component['_consecutiveHealthErrors'] = 100;

    httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/health`
    }).flush({}, { status: 500, statusText: '' });

    tick(POLL_INTERVAL);

    spyOn(component.goToTab, 'emit');
    const dialog = component['_healthErrDialog'] as MatDialogRef<ConfirmationDialogData>;
    expect(dialog.getState()).toBe(MatDialogState.OPEN);
    expect(dialog.componentInstance.title).toBe('Activation Error');
    expect(dialog.componentInstance.content).toBe(
      'Unexpected failure, please try again. If you continue to have problems, try regenerating the package file.');
    expect(dialog.componentInstance.buttons[0].label).toBe('Try again');
    dialog.componentInstance.buttons[0].callback();
    expect(component.goToTab.emit).toHaveBeenCalledWith(TabIndex.UPLOAD);

    component.ngOnDestroy();
    flush();
  }));

  it('should show snackbar when in progress status detected', () => {
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush({});
    spyOn(component['_snackBar'], 'open').and.returnValue(snackbarRefSpy);
    component.pkgInfo = undefined;
    component.provisioningAlreadyInProgress = true;

    component.ngOnInit();
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush({});
    expect(component['_snackBar'].open).toHaveBeenCalled();
  });

  it('should hide snackbar when status passed ZTPCONNECTING', () => {
    const mockStatus: ProvisioningStatus = {
      state: ProvisioningStatusState.ZTPCONFIGURING
    };
    spyOn(component, 'dismissSnackbar').and.callThrough();
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush({});
    spyOn(component['_snackBar'], 'open').and.returnValue(snackbarRefSpy);
    component.pkgInfo = undefined;
    component.provisioningAlreadyInProgress = true;

    component.ngOnInit();
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush(mockStatus);
    expect(component.dismissSnackbar).toHaveBeenCalled();
  });

  it('should not show snackbar when showRestartSnackbar is false', () => {
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush({});
    spyOn(component['_snackBar'], 'open').and.returnValue(snackbarRefSpy);
    component.pkgInfo = undefined;
    component.provisioningAlreadyInProgress = true;
    component.showRestartSnackbar = false;

    component.ngOnInit();
    httpTestingController.expectOne(`${environment.restServer}/v1/status`).flush({});
    expect(component['_snackBar'].open).not.toHaveBeenCalled();
  });
});
