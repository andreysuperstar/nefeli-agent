import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { UpgradeAgentResponse } from 'rest_client/nefagent/model/upgradeAgentResponse';
import { PackageError } from 'rest_client/nefagent/model/packageError';

import { HttpStatus } from 'src/app/http-status';

import { environment } from '../../../environments/environment';
import { BASE_PATH } from 'rest_client/nefagent/variables';

import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';

import { UpgradeAgentService } from 'rest_client/nefagent/api/upgradeAgent.service';
import { ActivateService } from 'rest_client/nefagent/api/activate.service';
import { HealthService } from 'rest_client/nefagent/api/health.service';

import { PackageInfo, PackageUploadComponent } from './package-upload.component';

describe('PackageUploadComponent', () => {
  let component: PackageUploadComponent;
  let fixture: ComponentFixture<PackageUploadComponent>;
  let httpTestingController: HttpTestingController;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatProgressSpinnerModule
      ],
      declarations: [PackageUploadComponent],
      providers: [
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        UpgradeAgentService,
        ActivateService,
        HealthService
      ]
    })
      .compileComponents();

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageUploadComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render illustration, welcome message and form', async () => {
    const illustrationDe = fixture.debugElement.query(By.css('img'));
    const messageDe = fixture.debugElement.query(By.css('p'));
    const formDe = fixture.debugElement.query(By.css('form'));

    expect(illustrationDe).not.toBeNull('render illustration');
    expect(messageDe).not.toBeNull('render welcome message');
    expect(formDe).not.toBeNull('render package upload form');

    const packageInputDe = formDe.query(By.css('input[type="file"]'));

    expect(packageInputDe).not.toBeNull('render package file input');

    await loader.getHarness(MatInputHarness.with({
      placeholder: 'Package file\'s passphrase'
    }));

    const provisionButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Provision'
    }));

    await expectAsync(provisionButton.isDisabled()).toBeResolvedTo(true);
  });

  it('should upload a package', async () => {
    const file: File = new File(['package'], 'andrew_site_2020-01-01_12-00-00.nefeli.pkg', {
      type: 'application/octet-stream',
    });

    const fileList: FileList = {
      length: 1,
      item: (index: number): File | null => file,
      0: file,
    };

    const event = {
      target: {
        files: fileList
      } as unknown
    } as Event;

    await component.onUploadPackage(event);

    expect(component.package).toBe('cGFja2FnZQ==', 'set a package data');

    const provisionButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Provision'
    }));

    await expectAsync(provisionButton.isDisabled()).toBeResolvedTo(false);
  });

  it('should activate up to date package version', async () => {
    const pkgData = 'cGFja2FnZQ==';
    const passphrase = 'theta';

    component['_package'] = pkgData;

    spyOn(component.activated, 'emit').and.callFake((pkg: PackageInfo) => {
      expect(pkg.disposition).toBe(1);
      expect(pkg.wantVersion).toBe('0.0.1');
      expect(pkg.sitePackage.encryptedPackage).toBe(pkgData);
      expect(pkg.sitePackage.passphrase).toBe(passphrase);
    });

    const passphraseInput = await loader.getHarness(MatInputHarness.with({
      placeholder: 'Package file\'s passphrase'
    }));

    const provisionButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Provision'
    }));

    passphraseInput.setValue(passphrase);

    spyOn(component, 'submitPackageUploadForm').and.callThrough();

    await provisionButton.click();

    expect(component.submitPackageUploadForm).toHaveBeenCalled();

    const upgradeAgentRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/upgradeagent`
    });

    upgradeAgentRequest.flush({
      disposition: UpgradeAgentResponse.DispositionEnum.NUMBER_1,
      haveVersion: '0.0.1',
      wantVersion: '0.0.1'
    } as UpgradeAgentResponse);

    expect(component.activated.emit).toHaveBeenCalled();
  });

  it('should render an error message on error response', async () => {
    component['_package'] = 'cGFja2FnZQ==';

    spyOn(component.activated, 'emit');

    const passphraseInput = await loader.getHarness(MatInputHarness.with({
      placeholder: 'Package file\'s passphrase'
    }));

    const provisionButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Provision'
    }));

    passphraseInput.setValue('theta');

    // Response with a 'Payment Required' error code
    await provisionButton.click();

    let upgradeAgentRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/upgradeagent`
    });

    let mockError: PackageError = {
      httpStatus: HttpStatus.BadRequest,
      message: 'Bad Request',
      errorCode: PackageError.ErrorCodeEnum.NUMBER_402
    };

    let mockErrorResponse: Partial<HttpErrorResponse> = {
      status: HttpStatus.BadRequest,
      statusText: 'Bad Request'
    };

    upgradeAgentRequest.flush(mockError, mockErrorResponse);
    fixture.detectChanges();

    expect(component['_packageUploadForm'].invalid).toBeTruthy('form is invalid');
    expect(component['_packageUploadForm'].errors?.upgradeAgentError).toBe(
      'The package or passphrase may be <strong>invalid</strong>, please try again.',
      'form has an upgrade agent error'
    );

    let errorParagraphDe = fixture.debugElement.query(By.css('p.error'));

    expect(errorParagraphDe.nativeElement.textContent).toBe(
      'The package or passphrase may be invalid, please try again.',
      'render an upgrade agent error'
    );

    expect(component.activated.emit).not.toHaveBeenCalled();

    // Response with a generic error
    await provisionButton.click();

    upgradeAgentRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/upgradeagent`
    });

    mockError = {
      httpStatus: HttpStatus.BadRequest,
      message: 'cannot decrypt package - crypto error: authentication failed',
      errorCode: PackageError.ErrorCodeEnum.NUMBER_403
    };

    mockErrorResponse = {
      status: HttpStatus.BadRequest,
      statusText: 'Bad Request'
    };

    upgradeAgentRequest.flush(mockError, mockErrorResponse);
    fixture.detectChanges();

    expect(component['_packageUploadForm'].invalid).toBeTruthy('form is invalid');
    expect(component['_packageUploadForm'].errors?.upgradeAgentError).toBe(
      'Incorrect passphrase, please try again.',
      'form has an upgrade agent error'
    );

    errorParagraphDe = fixture.debugElement.query(By.css('p.error'));

    expect(errorParagraphDe.nativeElement.textContent).toBe(
      'Incorrect passphrase, please try again.',
      'render an upgrade agent error'
    );

    expect(component.activated.emit).not.toHaveBeenCalled();
  });
});
