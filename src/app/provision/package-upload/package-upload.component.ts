import { Component, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { HttpStatus } from 'src/app/http-status';

import { SitePackage } from 'rest_client/nefagent/model/sitePackage';
import { UpgradeAgentResponse } from 'rest_client/nefagent/model/upgradeAgentResponse';
import { PackageError } from 'rest_client/nefagent/model/packageError';

import { UpgradeAgentService } from 'rest_client/nefagent/api/upgradeAgent.service';

export interface PackageInfo {
  disposition: UpgradeAgentResponse.DispositionEnum | undefined;
  wantVersion: string | undefined;
  sitePackage: SitePackage;
}

@Component({
  selector: 'nefagent-package-upload',
  templateUrl: './package-upload.component.html',
  styleUrls: ['./package-upload.component.less']
})
export class PackageUploadComponent {
  private _packageUploadForm: FormGroup = this._fb.group({
    passphrase: [undefined, Validators.required]
  });

  private _package: string | undefined = undefined;
  private _isUpgrading = false;
  private _activated = new EventEmitter<PackageInfo>();
  private _packageUploadSubscription: Subscription | undefined = undefined;

  @Output() public get activated(): EventEmitter<PackageInfo> {
    return this._activated;
  }

  constructor(
    private _fb: FormBuilder,
    private _upgradeAgent: UpgradeAgentService
  ) { }

  private uploadFile(event: Event): Promise<string> {
    const target = event.target as HTMLInputElement;

    const fileList = target.files;
    const file = fileList?.item(0);

    this._package = undefined;

    if (!file) {
      return Promise.reject();
    }

    const fileReader = new FileReader();

    return new Promise((resolve) => {
      fileReader.addEventListener('load', (progressEvent: ProgressEvent) => {
        fileReader.removeEventListener('load', () => { }, false);

        const base64EncodedData = (progressEvent.target as FileReader).result as string;

        resolve(base64EncodedData);
      }, false);

      fileReader.readAsDataURL(file);
    });
  }

  public async onUploadPackage(event: Event): Promise<void> {
    try {
      const base64EncodedData = await this.uploadFile(event);

      const encodedData = base64EncodedData
        // removing a base64 part 'data:application/octet-stream;base64,'
        .substr(base64EncodedData.indexOf(',') + 1)
        // removing a left base64 part 'data:' in case of empty file's content
        .replace('data:', '');

      this._package = encodedData;

      this._packageUploadForm.updateValueAndValidity();
    } catch { }
  }

  public submitPackageUploadForm(): void {
    this._packageUploadForm.updateValueAndValidity();

    const { invalid } = this._packageUploadForm;

    if (invalid) {
      return;
    }

    const { passphrase }: {
      passphrase: string
    } = this._packageUploadForm.value;

    const sitePackage: SitePackage = {
      encryptedPackage: this._package,
      passphrase
    };

    if (this._packageUploadSubscription) {
      this._packageUploadSubscription.unsubscribe();
    }

    this._isUpgrading = true;

    this._packageUploadSubscription = this._upgradeAgent
      .postUpgradeAgent(sitePackage)
      .pipe(
        finalize(() => {
          this._isUpgrading = false;
        })
      )
      .subscribe({
        next: ({ disposition, wantVersion }: UpgradeAgentResponse) => {
          this._activated.emit({ disposition, wantVersion, sitePackage });
        },
        error: ({
          error: { httpStatus, errorCode }
        }: HttpErrorResponse) => {
          let upgradeAgentError = 'An unexpected error has occurred, please try again.';

          if (httpStatus === HttpStatus.BadRequest) {
            switch (errorCode) {
              case PackageError.ErrorCodeEnum.NUMBER_402:
                upgradeAgentError = 'The package or passphrase may be <strong>invalid</strong>, please try again.';
                break;
              case PackageError.ErrorCodeEnum.NUMBER_403:
                upgradeAgentError = 'Incorrect passphrase, please try again.';
                break;
            }
          }

          this._packageUploadForm.setErrors({ upgradeAgentError });
        }
      });
  }

  public get packageUploadForm(): FormGroup {
    return this._packageUploadForm;
  }

  public get package(): string | undefined {
    return this._package;
  }

  public get isUpgrading(): boolean {
    return this._isUpgrading;
  }
}
