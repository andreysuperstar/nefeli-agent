import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletionComponent, MODE } from './completion.component';
import { MatToolbarModule } from '@angular/material/toolbar';

describe('CompletionComponent', () => {
  let component: CompletionComponent;
  let fixture: ComponentFixture<CompletionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatToolbarModule
      ],
      declarations: [ CompletionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should properly display based on inputs', () => {
    const el = fixture.debugElement.nativeElement;

    component.title = 'Success!';
    component.description = 'This is a test description';
    component.mode = MODE.SUCCESS;
    fixture.detectChanges();

    expect(el.querySelector('img').src).toContain('assets/ucpe-setup-complete.svg');
    expect(el.querySelector('h2').textContent).toBe('Success!');
    expect(el.querySelector('h5').textContent).toBe('This is a test description');

    component.title = 'Error!';
    component.description = 'This is a test description';
    component.mode = MODE.ERROR;
    fixture.detectChanges();

    expect(el.querySelector('img').src).toContain('assets/ucpe-setup-error.svg');
    expect(el.querySelector('h2').textContent).toBe('Error!');
    expect(el.querySelector('h5').textContent).toBe('This is a test description');
  });
});
