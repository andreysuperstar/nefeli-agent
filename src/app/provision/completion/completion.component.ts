import { Component, Input, OnInit } from '@angular/core';

export enum TitleType {
  NONE = '',
  SUCCESS = 'Complete',
  ERROR = 'Failed'
}

export enum DescriptionType {
  NONE = '',
  SUCCESS = 'You may now deploy services on this device using Nefeli Weaver.',
  ERROR = 'Could not provision device.',
  PROVISIONED = 'This device has already been provisioned.'
}

export enum MODE {
  SUCCESS,
  ERROR,
  NONE
}

@Component({
  selector: 'nefagent-completion',
  templateUrl: './completion.component.html',
  styleUrls: ['./completion.component.less']
})
export class CompletionComponent {

  private _title = '';
  private _description = '';
  private _mode = MODE.NONE;

  constructor() { }

  @Input() public set title(val: string) {
    this._title = val;
  }

  public get title(): string {
    return this._title;
  }

  @Input() public set description(val: string) {
    this._description = val;
  }

  public get description(): string {
    return this._description;
  }

  @Input() public set mode(val: MODE) {
    this._mode = val;
  }

  public get icon(): string {
    switch (this._mode) {
      case MODE.SUCCESS:
        return '../../../assets/ucpe-setup-complete.svg';
      case MODE.ERROR:
        return '../../../assets/ucpe-setup-error.svg';
      default:
        return '';
    }
  }
}
