import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { BASE_PATH } from 'rest_client/nefagent/variables';
import { environment } from '../../environments/environment';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { ProvisionService } from './provision.service';

import { ToolbarComponent } from '../toolbar/toolbar.component';
import { TabIndex, ProvisionComponent } from './provision.component';
import { PackageUploadComponent } from './package-upload/package-upload.component';
import { StatusComponent, StatusCompleteEvent } from './status/status.component';
import { TitleType, DescriptionType, MODE, CompletionComponent } from './completion/completion.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ProvisioningStatusState } from 'rest_client/nefagent/model/provisioningStatusState';
import { ProvisioningStatus } from 'rest_client/nefagent/model/provisioningStatus';

describe('ProvisionComponent', () => {
  let component: ProvisionComponent;
  let fixture: ComponentFixture<ProvisionComponent>;
  let el: HTMLElement;
  let httpTestingController: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatToolbarModule,
        MatTabsModule,
        MatCardModule,
        MatTabsModule,
        MatStepperModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatInputModule,
        MatDialogModule,
        MatSnackBarModule
      ],
      declarations: [
        ToolbarComponent,
        ProvisionComponent,
        PackageUploadComponent,
        StatusComponent,
        CompletionComponent,
      ],
      providers: [
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        ProvisionService
      ]
    })
      .compileComponents();

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select Status tab after package activation', waitForAsync(() => {
    const uploadPackageDe = fixture.debugElement.query(By.css('nefagent-package-upload'));

    expect(component['_tabGroup']?.selectedIndex).toBe(TabIndex.UPLOAD);

    spyOn(component, 'onActivated').and.callThrough();

    uploadPackageDe.triggerEventHandler('activated', undefined);
    fixture.detectChanges();

    expect(component.onActivated).toHaveBeenCalled();

    expect(component['_tabGroup']?.selectedIndex).toBe(TabIndex.STATUS);

    fixture
      .whenStable()
      .then(() => {
        expect(fixture.debugElement.query(By.css('nefagent-status'))).not.toBeNull('render Status tab');
      });
  }));

  it('should handle success mode event', waitForAsync(() => {
    const event: StatusCompleteEvent = { mode: MODE.SUCCESS };
    component.onStatusComplete(event);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const completionEl = el.querySelector('nefagent-completion');
      expect(completionEl).not.toBeNull();
      fixture.detectChanges();
      expect(completionEl?.querySelector('h2')?.textContent).toBe('Complete');
      expect(completionEl?.querySelector('h5')?.textContent).toBe('You may now deploy services on this device using Nefeli Weaver.');
    });
  }));

  it('should handle error mode', waitForAsync(() => {
    const event: StatusCompleteEvent = { mode: MODE.ERROR };
    component.onStatusComplete(event);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const completionEl = el.querySelector('nefagent-completion');
      fixture.detectChanges();
      expect(completionEl?.querySelector('h2')?.textContent).toBe('Failed');
      expect(completionEl?.querySelector('h5')?.textContent).toBe('Could not provision device.');
    });
  }));

  it('should redirect to Complete page for already provisioned unit', waitForAsync(() => {
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/status`);

    const mockProvisioningStatus: ProvisioningStatus = {
      state: ProvisioningStatusState.ZTPSUCCESS
    };

    request.flush(mockProvisioningStatus);
    fixture.detectChanges();

    expect(component['_tabGroup']?.selectedIndex).toBe(
      TabIndex.COMPLETE,
      'select Complete tab'
    );

    fixture
      .whenStable()
      .then(() => {
        const completionDe = fixture.debugElement.query(By.css('nefagent-completion'));

        expect(completionDe).not.toBeNull('render Completion component');

        fixture.detectChanges();

        const titleDe = completionDe.query(By.css('h2'));
        const descriptionDe = completionDe.query(By.css('h5'));

        expect(titleDe.nativeElement.textContent).toBe(
          TitleType.SUCCESS,
          `render success title text`
        );
        expect(descriptionDe.nativeElement.textContent).toBe(
          DescriptionType.PROVISIONED,
          'render provision description'
        );
      });
  }));

  it('should make status request on init', waitForAsync(() => {
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    expect(request.request.method).toBe('GET');
    const mockStatus: ProvisioningStatus = {
      state: ProvisioningStatusState.ACTIVATIONINITIATED
    };
    request.flush(mockStatus);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.provisioningAlreadyInProgress).toBe(true);
      expect(component.showRestartSnackbar).toBe(true);
      expect(component['_tabGroup']?.selectedIndex).toBe(TabIndex.STATUS);
    });
  }));

  it('should make not redirect if status is NULLPROVISIONINGSTATE', waitForAsync(() => {
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    const mockStatus: ProvisioningStatus = {
      state: ProvisioningStatusState.NULLPROVISIONINGSTATE
    };
    request.flush(mockStatus);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.provisioningAlreadyInProgress).toBe(false);
      expect(component['_tabGroup']?.selectedIndex).toBe(TabIndex.UPLOAD);
    });
  }));

  it('should handle fatal status', waitForAsync(() => {
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    const mockStatus: ProvisioningStatus = {
      state: ProvisioningStatusState.ZTPFATAL
    };
    request.flush(mockStatus);

    fixture.detectChanges();
    expect(component.provisioningAlreadyInProgress).toBe(false);
    expect(component['_tabGroup']?.selectedIndex).toBe(TabIndex.UPLOAD);
  }));

  it('should make not redirect if status is passed ZTPCONNECTING', waitForAsync(() => {
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/status`);
    const mockStatus: ProvisioningStatus = {
      state: ProvisioningStatusState.ZTPCONFIGURING
    };
    request.flush(mockStatus);
    fixture.detectChanges();

    expect(component.provisioningAlreadyInProgress).toBe(true);
    expect(component.showRestartSnackbar).toBe(false);
    expect(component['_tabGroup']?.selectedIndex).toBe(TabIndex.STATUS);
  }));
});
