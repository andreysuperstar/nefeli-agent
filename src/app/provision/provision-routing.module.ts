import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProvisionComponent } from './provision.component';

const routes: Routes = [
  {
    path: '',
    component: ProvisionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProvisionRoutingModule { }
