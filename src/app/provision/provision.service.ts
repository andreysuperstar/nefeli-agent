import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { shareReplay, repeatWhen, delay } from 'rxjs/operators';
import { StatusService } from 'rest_client/nefagent/api/status.service';
import { ProvisioningStatus } from 'rest_client/nefagent/model/provisioningStatus';
import { ProvisioningStatusState } from 'rest_client/nefagent/model/provisioningStatusState';

const Config = {
  SERVER_POLL_INTERVAL: 3000
};

@Injectable({
  providedIn: 'root'
})
export class ProvisionService {

  constructor(
    private _restStatusService: StatusService
  ) { }

  public streamStatus(): Observable<ProvisioningStatus> {
    return this._restStatusService
      .getStatus()
      .pipe(
        repeatWhen(notifications => notifications.pipe(
          delay(Config.SERVER_POLL_INTERVAL)
        )),
        shareReplay({
          refCount: true
        })
      );
  }

  public getStatus(): Observable<ProvisioningStatus> {
    return this._restStatusService.getStatus();
  }

  public isPassedConnectingState(state: ProvisioningStatusState): boolean {
    return ![
      ProvisioningStatusState.NULLPROVISIONINGSTATE,
      ProvisioningStatusState.ACTIVATIONINITIATED,
      ProvisioningStatusState.BOOTSTRAPINSTALLED,
      ProvisioningStatusState.ZTPCONNECTING
    ].includes(state);
  }
}
