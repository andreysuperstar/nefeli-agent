import { Subscription } from 'rxjs';
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { MatTabGroup } from '@angular/material/tabs';

import { MODE, DescriptionType, TitleType } from './completion/completion.component';
import { PackageInfo } from './package-upload/package-upload.component';
import { ProvisionService } from './provision.service';
import { StatusCompleteEvent } from './status/status.component';
import { ProvisioningStatus } from 'rest_client/nefagent/model/provisioningStatus';
import { ProvisioningStatusState } from 'rest_client/nefagent/model/provisioningStatusState';

export enum TabIndex {
  UPLOAD = 0,
  STATUS = 1,
  COMPLETE = 2
}

interface Complete {
  mode: MODE;
  title: TitleType;
  description: DescriptionType;
}

@Component({
  selector: 'nefagent-provision',
  templateUrl: './provision.component.html',
  styleUrls: ['./provision.component.less']
})
export class ProvisionComponent implements OnInit, OnDestroy {
  @ViewChild('tabGroup', {
    static: true
  }) private _tabGroup?: MatTabGroup;

  private _subscription = new Subscription();
  private _complete = {
    mode: MODE.NONE,
    title: TitleType.NONE,
    description: DescriptionType.NONE
  };
  private _pkgInfo: PackageInfo | undefined;
  private _provisioningAlreadyInProgress = false;
  private _showRestartSnackbar = true;

  constructor(
    private _provisionService: ProvisionService
  ) { }

  public ngOnInit(): void {
    this.checkProvisioningInProgress();
  }

  private checkProvisioningInProgress(): void {
    const subscription: Subscription = this._provisionService
      .getStatus()
      .subscribe((status: ProvisioningStatus) => {
        const state: ProvisioningStatusState | undefined = status.state;
        switch (state) {
          case ProvisioningStatusState.ACTIVATIONINITIATED:
          case ProvisioningStatusState.BOOTSTRAPINSTALLED:
          case ProvisioningStatusState.ZTPCONNECTING:
          case ProvisioningStatusState.ZTPINSTALLING:
          case ProvisioningStatusState.ZTPCONFIGURING:
          case ProvisioningStatusState.ZTPROLLINGBACK:
            this._provisioningAlreadyInProgress = true;
            if (this._provisionService.isPassedConnectingState(state)) {
              this._showRestartSnackbar = false;
            }
            if (this._tabGroup) {
              this._tabGroup.selectedIndex = TabIndex.STATUS;
            }
            break;
          case ProvisioningStatusState.ZTPSUCCESS:
            if (this._tabGroup) {
              this._tabGroup.selectedIndex = TabIndex.COMPLETE;

              this._complete = {
                mode: MODE.NONE,
                title: TitleType.SUCCESS,
                description: DescriptionType.PROVISIONED
              };
            }
        }
      });
    this._subscription.add(subscription);
  }



  public ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }

  public onActivated(pkg: PackageInfo): void {
    this._pkgInfo = pkg;

    if (this._tabGroup) {
      this._tabGroup.selectedIndex = TabIndex.STATUS;
    }
  }

  public onStatusComplete(event: StatusCompleteEvent): void {
    if (this._tabGroup) {
      switch (event.mode) {
        case MODE.SUCCESS: {
          this._tabGroup.selectedIndex = TabIndex.COMPLETE;
          this._complete.mode = MODE.SUCCESS;
          this._complete.title = TitleType.SUCCESS;
          this._complete.description = DescriptionType.SUCCESS;
          break;
        }
        case MODE.ERROR: {
          this._tabGroup.selectedIndex = TabIndex.COMPLETE;
          this._complete.mode = MODE.ERROR;
          this._complete.title = TitleType.ERROR;
          this._complete.description = DescriptionType.ERROR;
          break;
        }
        default:
          this._tabGroup.selectedIndex = TabIndex.UPLOAD;
      }
    }
  }

  public goToTab(index: TabIndex): void {
    if (this._tabGroup) {
      this._tabGroup.selectedIndex = index;
    }
  }

  public get complete(): Complete {
    return this._complete;
  }

  public get MODE(): typeof MODE {
    return MODE;
  }

  public get pkgInfo(): PackageInfo | undefined {
    return this._pkgInfo;
  }

  public get provisioningAlreadyInProgress(): boolean {
    return this._provisioningAlreadyInProgress;
  }

  public get showRestartSnackbar(): boolean {
    return this._showRestartSnackbar;
  }
}
