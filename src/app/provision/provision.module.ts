import { ProvisionService } from './provision.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatStepperModule } from '@angular/material/stepper';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { ProvisionRoutingModule } from './provision-routing.module';

import { ProvisionComponent } from './provision.component';
import { PackageUploadComponent } from './package-upload/package-upload.component';
import { StatusComponent } from './status/status.component';
import { CompletionComponent } from './completion/completion.component';



@NgModule({
  declarations: [
    ProvisionComponent,
    PackageUploadComponent,
    StatusComponent,
    CompletionComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatCardModule,
    MatTabsModule,
    MatStepperModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    ProvisionRoutingModule,
    MatSnackBarModule
  ],
  providers: [
    ProvisionService
  ]
})
export class ProvisionModule { }
