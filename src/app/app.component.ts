import { Component } from '@angular/core';

@Component({
  selector: 'nefagent-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent { }
