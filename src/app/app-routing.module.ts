import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'provision',
    loadChildren: () => import('./provision/provision.module').then(m => m.ProvisionModule)
  },
  {
    path: '',
    redirectTo: '/provision',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
