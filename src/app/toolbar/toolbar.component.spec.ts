import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ToolbarComponent } from './toolbar.component';

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatToolbarModule
      ],
      declarations: [ ToolbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render toolbar properly', () => {
    const el = fixture.debugElement.nativeElement;
    const nefIcon: HTMLImageElement = el.querySelector('mat-toolbar > img');
    const helpButton: HTMLAnchorElement = el.querySelector('mat-toolbar > a');

    expect(nefIcon).toBeDefined();
    expect(helpButton).toBeDefined();
  });
});
