import { create, defaults } from 'json-server';
import { ROUTES } from './routes';

const PORT = 3000;

const server = create();
const middlewares = defaults();

ROUTES.forEach((route) => {

  if (typeof route.response === 'function') {
    server[route.method](route.path, route.response);
  } else {
    server[route.method](route.path, (req, res) => {
      res.status(route.status as number).jsonp(route.response);
    });
  }
});

server.use(middlewares);
server.listen(PORT, () => {
  console.log(`JSON Server is running on port ${PORT}`);
});
