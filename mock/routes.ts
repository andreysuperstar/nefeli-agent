import { Activate, ActivateAuthenticationErr, ActivateErr, ActivatePackageErr } from './data/activate';
import { Health } from './data/health';
import { Status } from './data/status';
import { UpgradeAgent } from './data/upgradeagent';

enum METHODS {
  GET = 'get',
  POST = 'post'
}

interface Route {
  method: METHODS;
  path: string;
  status?: number;
  response: any;
}

export const ROUTES: Route[] = [
  { method: METHODS.GET, path: '/v1/health', status: 200, response: Health },
  { method: METHODS.GET, path: '/v1/status', response: Status },
  { method: METHODS.POST, path: '/v1/upgradeagent', status: 200, response: UpgradeAgent },

  { method: METHODS.POST, path: '/v1/activate', status: 200, response: Activate }
  // { method: METHODS.POST, path: '/v1/activate', status: 400, response: ActivatePackageErr }
  // { method: METHODS.POST, path: '/v1/activate', status: 401, response: ActivateAuthenticationErr }
  // { method: METHODS.POST, path: '/v1/activate', status: 422, response: ActivateErr }
];
