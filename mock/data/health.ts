import { AgentHealth } from '../../rest_client/nefagent/model/agentHealth';

export const Health: AgentHealth = {
  status: 'success',
  version: '0.0.1'
};
