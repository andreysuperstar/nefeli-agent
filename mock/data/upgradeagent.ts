import { UpgradeAgentResponse } from '../../rest_client/nefagent/model/upgradeAgentResponse';

export const UpgradeAgent: UpgradeAgentResponse = {
  disposition: 1,
  haveVersion: '0.0.1',
  wantVersion: '0.0.2'
};
