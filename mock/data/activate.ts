import { ActivateError } from '../../rest_client/nefagent/model/activateError';
import { AuthenticationError } from '../../rest_client/nefagent/model/authenticationError';
import { PackageError } from '../../rest_client/nefagent/model/packageError';
import { DefaultResponse } from '../../rest_client/nefagent/model/defaultResponse';

export const Activate: DefaultResponse = {
  status: DefaultResponse.StatusEnum.Success
};

export const ActivatePackageErr: PackageError = {
  httpStatus: 400,
  message: 'Generic unspecified error.',
  errorCode: PackageError.ErrorCodeEnum.NUMBER_400
};

export const ActivateAuthenticationErr: AuthenticationError = {
  httpStatus: 401,
  message: 'Invalid passphrase',
  errorCode: AuthenticationError.ErrorCodeEnum.NUMBER_200
};

export const ActivateErr: ActivateError = {
  httpStatus: 422,
  // httpStatus: 500,
  message: 'Generic unspecified error.',
  errorCode: ActivateError.ErrorCodeEnum.NUMBER_100
};
