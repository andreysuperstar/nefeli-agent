import { ProvisioningStatus } from '../../rest_client/nefagent/model/provisioningStatus';
import { ProvisioningStatusState } from '../../rest_client/nefagent/model/provisioningStatusState';

let num = 0;

const successTest = (_, res) => {
  const status: ProvisioningStatus = {
    additionalInfo: '',
    state: ProvisioningStatusState.NULLPROVISIONINGSTATE,
    version: `0.0.${num++}`
  };

  switch (num) {
    case 1:
      status.state = ProvisioningStatusState.NULLPROVISIONINGSTATE;
      res.status(200).jsonp(status);
      break;
    case 2:
      status.state = ProvisioningStatusState.ACTIVATIONINITIATED;
      res.status(200).jsonp(status);
      break;
    case 3:
      status.state = ProvisioningStatusState.BOOTSTRAPINSTALLED;
      res.status(200).jsonp(status);
      break;
    case 4:
      status.state = ProvisioningStatusState.ZTPCONNECTING;
      res.status(200).jsonp(status);
      break;
    case 5:
      status.state = ProvisioningStatusState.ZTPINSTALLING;
      res.status(200).jsonp(status);
      break;
    case 6:
      res.status(500).send(JSON.stringify({ error: 'Server error' }));
      break;
    case 7:
      status.state = ProvisioningStatusState.ZTPROLLINGBACK;
      res.status(200).jsonp(status);
      break;
    case 8:
      status.state = ProvisioningStatusState.ZTPCONFIGURING;
      res.status(200).jsonp(status);
      break;
    case 9:
      status.state = ProvisioningStatusState.ZTPSUCCESS;
      res.status(200).jsonp(status);
      break;
    default: {
      status.state = ProvisioningStatusState.NULLPROVISIONINGSTATE;
      res.status(200).jsonp(status);
      break;
    }
  }
};

/**
 * Mocks the case of where nefagent has problems connecting to the provisioning server.
 * It tries three different servers, but finally succeeds on the 8th poll.
 */
const provisioningServerConnectionFailure = (_, res) => {
  const connectFailure = 'ztp onboard unsuccessful, will retry';
  const provisioningServers = ['https://provision1.server.com', 'https://provision2.server.com', 'https://provision3.server.com'];
  const status: ProvisioningStatus = {
    additionalInfo: '',
    state: ProvisioningStatusState.NULLPROVISIONINGSTATE,
    version: `0.0.1`
  };

  switch (num++) {
    case 0:
      status.state = ProvisioningStatusState.ZTPCONNECTING;
      status.additionalInfo = provisioningServers[0];
      status.eventTime = new Date();
      res.status(200).jsonp(status);
      break;
    case 2:
      status.state = ProvisioningStatusState.ZTPCONNECTING;
      status.additionalInfo = provisioningServers[1];
      status.eventTime = new Date();
      res.status(200).jsonp(status);
      break;
    case 4:
      status.state = ProvisioningStatusState.ZTPCONNECTING;
      status.additionalInfo = provisioningServers[2];
      status.eventTime = new Date();
      res.status(200).jsonp(status);
      break;
    case 8:
      status.state = ProvisioningStatusState.ZTPINSTALLING;
      res.status(200).jsonp(status);
      break;
    case 9:
      status.state = ProvisioningStatusState.ZTPCONFIGURING;
      res.status(200).jsonp(status);
      break;
    case 10:
      status.state = ProvisioningStatusState.ZTPSUCCESS;
      res.status(200).jsonp(status);
      break;
    default:
      status.state = ProvisioningStatusState.ZTPCONNECTING;
      status.additionalInfo = connectFailure;
      status.eventTime = new Date();
      res.status(200).jsonp(status);
      break;
  }
};

export const Status = successTest;
